﻿using System;
namespace MA_Payroll.Dto
{
    public class ProduktDto
    {
        public string Produktname { get; set; }
        public string Produktlink { get; set; }
        public string ProduktIconLink { get; set; }
    }

}
