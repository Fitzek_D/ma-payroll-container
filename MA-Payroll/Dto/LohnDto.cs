﻿using System;
namespace MA_Payroll.Dto
{
    public class LohnDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public int Betrag { get; set; }
    }
}
